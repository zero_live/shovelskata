
describe('CodeSyntax', () => {
  it('validates that an opener shovel is followed by closer shovel', () => {
    const codeWithClosedShovel = '<>'

    const validation = CodeSyntax.validate(codeWithClosedShovel)

    expect(validation).toBe(true)
  })

  it('does not validate that an opener shovel is alone', () => {
    const codeWithOnlyAShovel = '<'

    const validation = CodeSyntax.validate(codeWithOnlyAShovel)

    expect(validation).toBe(false)
  })

  it('validates nested shovels', () => {
    const codeWithNestedShovels = '<<>>'

    const validation = CodeSyntax.validate(codeWithNestedShovels)

    expect(validation).toBe(true)
  })

  it('does not validate ><', () => {
    const code = '><'

    const validation = CodeSyntax.validate(code)

    expect(validation).toBe(false)
  })

  it('validates closed brackets', () => {
    const code = '[]'

    const validation = CodeSyntax.validate(code)

    expect(validation).toBe(true)
  })
})
