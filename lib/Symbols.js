
const Symbols = class {
  static _SHOVELS = {
    opener: '<',
    closer: '>'
  }
  static _BRACKETS = {
    opener: '[',
    closer: ']'
  }
  static _PAIRS = [this._SHOVELS, this._BRACKETS]

  static pairs() {
    return this._PAIRS
  }
}
