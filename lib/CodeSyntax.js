
const CodeSyntax = class {
  static validate(code) {
    const characters = Characters.from(code)

    const areEven = this._areEvenAllSymbolsIn(characters)
    const areInEdges = this._areSomeSymbolInEdgesIn(characters)

    const validation = areEven && areInEdges
    return validation
  }

  static _areEvenAllSymbolsIn(characters) {
    const areEven = Symbols.pairs().every(pair => characters.areEven(pair.opener, pair.closer))

    return areEven
  }

  static _areSomeSymbolInEdgesIn(characters) {
    const areInEdges = Symbols.pairs().some(pair => characters.areInEdges({ begining: pair.opener, end: pair.closer }))

    return areInEdges
  }
}
