const Characters = class {
  static from(code) {
    const characters = code.split('')

    return new Characters(characters)
  }

  constructor(characters) {
    this.characters = characters
  }

  areEven(aCharacter, anotherCharacter) {
    let aCount = this._count(aCharacter)
    let anotherCount = this._count(anotherCharacter)

    let areEven = (aCount === anotherCount)
    return areEven
  }

  areInEdges({ begining, end }) {
    return (this._isFirst(begining) || this._isLast(end))
  }

  _count(characterToCount) {
    let count = 0

    this.characters.forEach((character) => {
      if (character === characterToCount) {
        count += 1
      }
    })

    return count
  }

  _firstCharacter() {
    return this.characters[0]
  }

  _lastCharacter() {
    const lastIndex = this.characters.length - 1

    return this.characters[lastIndex]
  }

  _isFirst(character) {
    return this._firstCharacter() === character
  }

  _isLast(character) {
    return this._lastCharacter() === character
  }
}
