# CodeSyntax CodeKata

Esta kata trata de resolver el problema de desarrollar una librería que sea capaz de descubrir si el código que le pasan tiene un simbolo de cierre por cada simbolo de apertura "<" en este caso las shovels ">" y luego "[]".

## ¿Cómo ejecutar los tests?

Sencillamente abre el archivo `Verano.js` en tu navegador, lo que ejecutará los tests del archivo `spec/CodeSyntaxSpec.js`.

## Requisitos del sistema

- Cualquier navegador que soport ES6 o compatibles.
